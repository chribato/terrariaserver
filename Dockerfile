FROM alpine:edge

MAINTAINER Christian Drüke, <cdrueke@gmx.de>

RUN echo "http://nl.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
   apk update && \
   apk add mono openssl p7zip

# download tdsm server
RUN wget https://github.com/DeathCradle/Terraria-s-Dedicated-Server-Mod/releases/download/1.3.2.1-beta/TDSM.1321.7z;

RUN mkdir -p /srv/tdsm; \
 7za e TDSM.1321.7z -aoa -o/srv/tdsm;  \
 rm *.7z;

WORKDIR /srv/tdsm

COPY start-server.sh ./start-server.sh

RUN chmod 777 start-server.sh

ENTRYPOINT ["./start-server.sh"]

#Source Repository
#  https://bitbucket.org/chribato/terrariaserver

